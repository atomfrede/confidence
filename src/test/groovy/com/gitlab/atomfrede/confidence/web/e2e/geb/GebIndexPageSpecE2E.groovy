package com.gitlab.atomfrede.confidence.web.e2e.geb

import com.gitlab.atomfrede.confidence.web.e2e.geb.pages.GebIndexPage
import com.gitlab.atomfrede.confidence.web.e2e.geb.pages.GebLoginPage
import geb.spock.GebReportingSpec

class GebIndexPageSpecE2E extends GebReportingSpec {

  def "admin text is displayed"() {

    given:
    to GebLoginPage

    when:
    loginPage.login("admin", "admin")

    then:
    find { $(".e2e__admin-text") }
  }

  def "can logout"() {

    given: "user is at index page"
    to GebLoginPage
    loginPage.login("admin", "admin")
    at GebIndexPage

    when:
    navbar.logout()

    then:
    at GebLoginPage
  }

  def "can change language"() {

    given: "user is at index page"
    to GebLoginPage
    loginPage.login("admin", "admin")
    at GebIndexPage

    and:
    $(".e2e__logout-button--text").text() == "Abmelden"

    when:
    go "/index?lang=en"

    then:
    at GebIndexPage
    $(".e2e__logout-button--text").text() == "Logout"
  }
}

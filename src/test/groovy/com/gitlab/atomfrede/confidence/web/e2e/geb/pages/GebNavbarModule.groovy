package com.gitlab.atomfrede.confidence.web.e2e.geb.pages

import geb.Module

class GebNavbarModule extends Module{

  static content = {
    logoutButton  { $(class: "e2e__logout-button") }
    currentLanguage { $(class: "e2e__current-language") }
    languages { $(class: "e2e__language-select") }
  }

  void logout() {
    logoutButton.click()
  }

  void changeLanguage() {
    currentLanguage.click()
    languages.click()
  }
}

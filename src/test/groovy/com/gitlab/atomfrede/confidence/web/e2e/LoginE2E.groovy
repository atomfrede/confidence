package com.gitlab.atomfrede.confidence.web.e2e

import com.gitlab.atomfrede.confidence.TestPaths
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.openqa.selenium.OutputType
import org.openqa.selenium.firefox.FirefoxOptions
import org.openqa.selenium.remote.RemoteWebDriver
import org.openqa.selenium.support.ui.WebDriverWait
import org.testcontainers.containers.BrowserWebDriverContainer
import org.testcontainers.containers.GenericContainer
import org.testcontainers.containers.Network
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.images.builder.ImageFromDockerfile
import org.testcontainers.shaded.org.apache.commons.io.FileUtils

import static org.assertj.core.api.Assertions.assertThat

class LoginE2E {

  private int counter = 0
  private Network network = Network.SHARED

  @Rule
  public PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer("postgres:11.0")
    .withDatabaseName("confidence")
    .withUsername("confidence")
    .withPassword("confidence")
    .withNetwork(network)
    .withNetworkAliases("postgres") as PostgreSQLContainer

  @Rule
  public BrowserWebDriverContainer firefox = new BrowserWebDriverContainer()
    .withCapabilities(new FirefoxOptions())
    .withRecordingMode(BrowserWebDriverContainer.VncRecordingMode.RECORD_ALL, new File("./build/")) // enable to record videos of all sequences
    .withNetwork(network)
    .withNetworkAliases("selenium") as BrowserWebDriverContainer

  private GenericContainer bootJar

  private RemoteWebDriver driver
  private WebDriverWait wait


  @Before
  void setup() {

    bootJar = new GenericContainer(new ImageFromDockerfile().withDockerfileFromBuilder(
      { builder ->
        builder.from("openjdk:11-jdk-stretch")
          .volume("/tmp")
          .copy("confidence-0.0.1-SNAPSHOT.jar", "app.jar")
          .entryPoint("java", "-Dspring.datasource.password=confidence",
            "-Dspring.datasource.url=jdbc:postgresql://postgres:5432/confidence",
            "-Djava.security.egd=file:/dev/./urandom", "-jar", "/app.jar")
          .build()
      })
      .withFileFromPath("confidence-0.0.1-SNAPSHOT.jar", TestPaths.resolve("../../../build/libs/confidence-0.0.1-SNAPSHOT.jar")))
      .withExposedPorts(8080)
      .withNetwork(network)
      .withNetworkAliases("confidence")

    bootJar.start()

    driver = firefox.getWebDriver()
    wait = new WebDriverWait(driver, 20)

    driver.manage().window().maximize()
  }


  @Test
  void userAndAdminScenario() {

    driver.get("http://confidence:8080/login")
    captureScreen("login-page")

    driver.findElementByClassName("e2e__username").sendKeys("admin")
    driver.findElementByClassName("e2e__password").sendKeys("admin")
    driver.findElementByClassName("e2e__login-button").click()

    captureScreen("index-page-as-admin")

    assertThat(driver.findElementByClassName("e2e__admin-text")).isNotNull()
    driver.findElementByClassName("e2e__logout-button").click()

    driver.findElementByClassName("e2e__username").sendKeys("user")
    driver.findElementByClassName("e2e__password").sendKeys("user")
    driver.findElementByClassName("e2e__login-button").click()

    captureScreen("index-page-as-user")
    assertThat(driver.findElementByClassName("e2e__user-text")).isNotNull()

  }

  private void captureScreen(String filename) throws IOException {

    String fullPathname =  "./build/" + String.format("%02d", counter++) + "_" + filename;

    FileUtils.copyFile(
      driver.getScreenshotAs(OutputType.FILE),
      new File(fullPathname)
    );
  }
}

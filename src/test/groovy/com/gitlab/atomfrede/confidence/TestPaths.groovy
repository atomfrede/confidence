package com.gitlab.atomfrede.confidence

import java.nio.file.Path

class TestPaths {

  private TestPaths() {
  }

  static Path resolve(String relativePathFromTargetFolder) {

    def url = TestPaths.class.getProtectionDomain().getCodeSource().getLocation()

    Path path

    try {
      path = new File(url.toURI()).toPath().resolve("../${relativePathFromTargetFolder}").normalize()
    } catch (URISyntaxException e) {
      throw new RuntimeException("Could not build path to '${relativePathFromTargetFolder}'", e)
    }
    File file = path.toFile()

    if (!file.isFile() || file.length() == 0) {
      throw new RuntimeException("File '${file}' is either non-existent or contains zero-bytes.")
    }

    return path
  }
}
package com.gitlab.atomfrede.confidence.web.e2e.geb.pages

import geb.Page

class GebLoginPage extends Page {

  static url = "/login"

  static at = { title == "Anmeldung - Gaining Confidence" }

  static content = {
    loginPage { module(GebLoginPageModule) }
  }
}

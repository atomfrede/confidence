package com.gitlab.atomfrede.confidence

import com.atlassian.oai.validator.OpenApiInteractionValidator
import com.atlassian.oai.validator.whitelist.ValidationErrorsWhitelist

import com.atlassian.oai.validator.restassured.OpenApiValidationFilter
import org.jooq.tools.json.JSONObject
import org.yaml.snakeyaml.Yaml

import static com.atlassian.oai.validator.whitelist.rule.WhitelistRules.allOf
import static com.atlassian.oai.validator.whitelist.rule.WhitelistRules.messageContains

class TestFilters {

  OpenApiValidationFilter swaggerValidationFilter

  TestFilters(String swaggerFileLocation) {

    def file = new File(swaggerFileLocation)

    def map = file.withReader {
      Yaml yaml = new Yaml()
      return yaml.load(it)
    }

    def whitelist = ValidationErrorsWhitelist.create()
      .withRule("Ignore 'application/problem+json' does not match any allowed types.",
      allOf(messageContains("Response Content-Type header 'application/problem\\+json' does not match any allowed types.*")))

    def interactionValidator = OpenApiInteractionValidator.createFor(new JSONObject(map).toString())
      .withWhitelist(whitelist)
      .build()

    swaggerValidationFilter = new OpenApiValidationFilter(interactionValidator)
  }
}

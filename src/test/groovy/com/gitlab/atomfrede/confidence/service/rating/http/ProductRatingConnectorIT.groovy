package com.gitlab.atomfrede.confidence.service.rating.http

import com.gitlab.atomfrede.confidence.http.OkHttpClientFactory
import io.specto.hoverfly.junit.api.HoverflyClient
import io.specto.hoverfly.junit.core.HoverflyMode
import io.specto.hoverfly.junit.core.model.RequestFieldMatcher
import org.junit.BeforeClass
import org.junit.ClassRule
import org.junit.Test
import org.testcontainers.containers.GenericContainer

import static io.specto.hoverfly.junit.core.SimulationSource.dsl
import static io.specto.hoverfly.junit.dsl.HoverflyDsl.service
import static io.specto.hoverfly.junit.dsl.ResponseCreators.success
import static org.assertj.core.api.Assertions.assertThat

class ProductRatingConnectorIT {

  @ClassRule
  public static GenericContainer hoverfly = new GenericContainer("spectolabs/hoverfly")
    .withCommand("-webserver")
    .withExposedPorts(8888, 8500)

  @BeforeClass
  static void setUp() {

    setupSimulation()
  }

  static setupSimulation() {

    HoverflyClient hoverflyClient = HoverflyClient.custom()
      .host(hoverfly.getContainerIpAddress())
      .port(hoverfly.getMappedPort(8888))
      .build()

    hoverflyClient.setMode(HoverflyMode.SIMULATE)

    hoverflyClient.setSimulation(dsl(
      simulateServer(),
    ).getSimulation())

  }

  static simulateServer() {

    //language=json
    def successBody = '''[
  {
    "id": 1,
    "title": "Nice Product",
    "rating": 5,
    "description": "Greetings from Hoverfly",
    "productId": 1
  },
  {
    "id": 2,
    "title": "Totally Bad",
    "rating": 1,
    "description": "Greetings from Hoverfly",
    "productId": 1
  }
]'''

    return service("http://${hoverfly.getContainerIpAddress()}:${hoverfly.getMappedPort(8500)}")
      .get("/reviews")
      .queryParam("productId", RequestFieldMatcher.newExactMatcher("1"))
      .anyQueryParams()
      .willReturn(success().body(successBody))
  }

  @Test
  void shouldLoadRatings() {

    def subject = new ProductRatingConnector(new OkHttpClientFactory(), "http://${hoverfly.getContainerIpAddress()}:${hoverfly.getMappedPort(8500)}/")

    def result = subject.fetchProductRatings(1)

    assertThat(result).isNotNull()
    assertThat(result).isNotEmpty()
    assertThat(result).extracting("description", String.class).contains("Greetings from Hoverfly")
  }
}

package com.gitlab.atomfrede.confidence.web;

public final class Routes {

    private Routes() {
    }

    public static final String ACTIVE_NAV = "activeNav";

    public static final String ROOT = "/";
    public static final String INDEX = "/index";
    public static final String LOGIN = "/login";
}

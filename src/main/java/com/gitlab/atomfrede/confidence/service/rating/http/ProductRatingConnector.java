package com.gitlab.atomfrede.confidence.service.rating.http;

import com.gitlab.atomfrede.confidence.http.HttpClientFactory;
import com.gitlab.atomfrede.confidence.http.OkHttpClientFactory;
import net.jodah.failsafe.Failsafe;
import net.jodah.failsafe.RetryPolicy;
import org.springframework.stereotype.Component;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.io.IOException;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Component
public class ProductRatingConnector {

    private ProductRatingService serviceClient;

    private RetryPolicy<Object> retryPolicy = new RetryPolicy<>()
        .handle(IOException.class)
        .withBackoff(1, 60, ChronoUnit.SECONDS)
        .withMaxRetries(3);

    public ProductRatingConnector() {
        this(new OkHttpClientFactory(), "https://my-json-server.typicode.com/atomfrede/json-faker-server-data/");
    }

    ProductRatingConnector(HttpClientFactory httpClientFactory, String baseUrl) {

        Retrofit retrofit = new Retrofit.Builder()
            .client(httpClientFactory.buildClient())
            .addConverterFactory(JacksonConverterFactory.create())
            .baseUrl(baseUrl)
            .build();

        serviceClient = retrofit.create(ProductRatingService.class);
    }

    public List<ProductRatingDto> fetchProductRatings(Long productId) {

        return Failsafe.with(retryPolicy).get(() -> fetchReviews(productId));
    }

    private List<ProductRatingDto> fetchReviews(Long productId) throws IOException {

        Response<List<ProductRatingDto>> response = serviceClient.findReviewsForProduct(productId).execute();

        if (response.isSuccessful()) {
            return response.body();
        }

        throw new RuntimeException("UnsuccessfulResponse.");
    }
}

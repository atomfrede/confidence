package com.gitlab.atomfrede.confidence.service.rating.http


import com.github.tomakehurst.wiremock.junit.WireMockRule
import com.gitlab.atomfrede.confidence.http.OkHttpClientFactory
import org.junit.Rule
import spock.lang.Specification

import static com.github.tomakehurst.wiremock.client.WireMock.*
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options

class ProductRatingConnectorSpec extends Specification {

  @Rule
  WireMockRule wireMockRule = new WireMockRule(options().dynamicPort())

  def "should load ratings"() {

    given:
    def subject = new ProductRatingConnector(new OkHttpClientFactory(), "http://localhost:${wireMockRule.port()}")

    and:
    //language=json
    def successBody = '''[
  {
    "id": 1,
    "title": "Nice Product",
    "rating": 5,
    "description": "Lorem Ipsum",
    "productId": 1
  },
  {
    "id": 2,
    "title": "Totally Bad",
    "rating": 1,
    "description": "Lorem Ipsum",
    "productId": 1
  }
]
'''
    and:
    stubFor(get(urlPathEqualTo("/reviews"))
      .withQueryParam("productId", equalTo("1"))
      .willReturn(aResponse()
      .withStatus(200)
      .withBody(successBody)))

    when:
    def result = subject.fetchProductRatings(1)

    then:
    result != null
    result.size() == 2

  }
}

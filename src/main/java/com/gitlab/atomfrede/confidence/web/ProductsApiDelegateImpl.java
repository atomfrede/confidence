package com.gitlab.atomfrede.confidence.web;

import com.gitlab.atomfrede.confidence.service.Product;
import com.gitlab.atomfrede.confidence.service.ProductService;
import com.gitlab.atomfrede.confidence.web.gen.api.ProductsApiDelegate;
import com.gitlab.atomfrede.confidence.web.gen.model.ProductCreateDto;
import com.gitlab.atomfrede.confidence.web.gen.model.ProductDto;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class ProductsApiDelegateImpl implements ProductsApiDelegate {

    private ProductService productService;
    private ProductDtoMapper dtoMapper = Mappers.getMapper(ProductDtoMapper.class);

    @Autowired
    public ProductsApiDelegateImpl(ProductService productService) {

        this.productService = productService;
    }

    @Override
    public ResponseEntity<ProductDto> createProduct(ProductCreateDto productCreateDto) {

        Product product = dtoMapper.fromCreateDto(productCreateDto);
        Product created = productService.createProduct(product);

        return new ResponseEntity<>(dtoMapper.toDto(created), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<ProductDto> getProduct(Integer id) {
        return ResponseEntity.ok(new ProductDto().id(id).description("Hello World").name("Thinkpad T480s"));
    }
}

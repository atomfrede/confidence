package com.gitlab.atomfrede.confidence.persistence

import com.gitlab.atomfrede.confidence.service.Product
import org.jooq.DSLContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jooq.JooqTest
import spock.lang.Specification

import static com.gitlab.atomfrede.confidence.persistence.gen.jooq.tables.ProductTable.PRODUCT_TABLE

@JooqTest
class JooqProductRepositoryIT extends Specification {

  @Autowired
  DSLContext jooq

  def cleanup() {
    new JooqProductRepository(jooq).clearAll()
  }

  def 'find product page'() {

    given:
    def subject = new JooqProductRepository(jooq)

    and:
    def product_1 = new Product(
      title: "Thinkpad T480s",
      description: "A thinkpad, as simple as that")

    def product_2 = new Product(
      title: "Thinkpad X1 Carbon",
      description: "A thinkpad, as simple as that, but with more style")

    and:
    subject.createProduct(product_1)
    subject.createProduct(product_2)

    when:
    def result = subject.findAll(0, 10)

    then:
    result != null
    result.getTotalCount() == 2
    result.getItems().size() == 2
  }
  def 'create product'() {

    given:
    def subject = new JooqProductRepository(jooq)

    and:
    def product = new Product(
      title: "Thinkpad T480s",
      description: "A thinkpad, as simple as that")

    when:
    def result = subject.createProduct(product)

    then:
    result != null
    result.isPresent()
    result.get().getId() != null
    result.get().getId() > 0
  }

  def 'fetch non existing product by id'() {

    given:
    def subject = new JooqProductRepository(jooq)

    when:
    def result = subject.findOneById(4711)

    then:
    result != null
    result.isEmpty()
  }

  def 'fetch existing product by id'() {

    given:
    def subject = new JooqProductRepository(jooq)

    and:
    def product = new Product(
      title: "Thinkpad T480s",
      description: "A thinkpad, as simple as that")

    and:
    def id = subject.createProduct(product).get().getId()

    when:
    def result = subject.findOneById(id)

    then:
    result != null
    result.isPresent()
    result.get().getId() != null
    result.get().getId() > 0
  }
}

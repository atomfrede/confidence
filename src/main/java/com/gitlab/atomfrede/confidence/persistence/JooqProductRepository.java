package com.gitlab.atomfrede.confidence.persistence;

import com.gitlab.atomfrede.confidence.Page;
import com.gitlab.atomfrede.confidence.persistence.gen.jooq.tables.records.ProductRecord;
import com.gitlab.atomfrede.confidence.service.Product;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.Select;
import org.jooq.SelectJoinStep;
import org.jooq.impl.DSL;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.gitlab.atomfrede.confidence.persistence.gen.jooq.tables.ProductTable.PRODUCT_TABLE;
import static java.util.Objects.nonNull;

@Component
public class JooqProductRepository implements ProductRepository {

    private static final String ALIAS_TOTAL_COUNT = "totalCount";

    private DSLContext jooq;
    private ProductRecordMapper recordMapper = Mappers.getMapper(ProductRecordMapper.class);

    @Autowired
    public JooqProductRepository(DSLContext jooq) {
        this.jooq = jooq;
    }

    @Override
    public Optional<Product> findOneById(Long id) {

        return jooq.selectFrom(PRODUCT_TABLE)
            .where(PRODUCT_TABLE.ID.eq(id))
            .fetchOptional()
            .map(record -> recordMapper.toProduct(record));
    }

    @Override
    public Optional<Product> createProduct(Product product) {

        return jooq.insertInto(PRODUCT_TABLE)
            .set(PRODUCT_TABLE.NAME, product.getTitle())
            .set(PRODUCT_TABLE.DESCRIPTION, product.getDescription())
            .returning()
            .fetchOptional()
            .map(recordMapper::toProduct);

    }

    public void clearAll() {

        this.jooq.deleteFrom(PRODUCT_TABLE).execute();
    }

    @Override
    public Page<Product> findAll(Integer offset, Integer limit) {

        final SelectJoinStep<Record> select = jooq
            .select(PRODUCT_TABLE.fields())
            .select(DSL.count().over().as(ALIAS_TOTAL_COUNT))
            .from(PRODUCT_TABLE);

        if (nonNull(limit)) {
            select.limit(limit);
        }

        if (nonNull(offset)) {
            select.offset(offset);
        }

        final List<Product> products = select
            .fetchInto(ProductRecord.class).stream()
            .map(recordMapper::toProduct)
            .collect(Collectors.toList());

        return Page.from(totalCountFrom(select), products);
    }

    private Integer totalCountFrom(final Select<Record> select) {

        final Result<Record> result = select.getResult();

        try {
            return result.getValue(0, result.field(ALIAS_TOTAL_COUNT, Integer.class));
        } catch (IndexOutOfBoundsException | IllegalArgumentException e) {
            // if there are not any results then we have a total count of 0
            return 0;
        }
    }
}

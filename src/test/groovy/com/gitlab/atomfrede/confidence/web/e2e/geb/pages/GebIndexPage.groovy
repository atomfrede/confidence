package com.gitlab.atomfrede.confidence.web.e2e.geb.pages

import geb.Page

class GebIndexPage extends Page {

  static url = "/"

  static at = { title == "Gaining Confidence" }

  static content = {
    navbar { module(GebNavbarModule) }
  }

}

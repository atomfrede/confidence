package com.gitlab.atomfrede.confidence.web

import com.gitlab.atomfrede.confidence.TestFilters
import com.gitlab.atomfrede.confidence.TestPaths
import com.gitlab.atomfrede.confidence.persistence.ProductRepository
import io.restassured.RestAssured
import io.restassured.http.ContentType
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort

import static io.restassured.RestAssured.given

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ProductsApiIT {

  @LocalServerPort
  private int port

  @Autowired
  private ProductRepository productRepository;

  @BeforeAll
  static void setup() throws IOException {

    RestAssured.reset()

    def testFilters = new TestFilters(TestPaths.resolve("../../../api-spec/server/products.yaml").toString())

    RestAssured.filters(
      testFilters.swaggerValidationFilter
    )

    RestAssured.enableLoggingOfRequestAndResponseIfValidationFails()
  }

  @BeforeEach
  void before() throws IOException {

    RestAssured.baseURI = "http://localhost:${port}/v1"
  }

  @AfterEach
  void cleanUp() {

    productRepository.clearAll()
  }

  @Test
  void get_product_page() {

  }

  @Test
  void create_product() {

    //language=json
    def productCreate = '''
{
  "name": "Thinkpad T480s",
  "description": "A thinkpad, as simple as that"
}
'''

    given()
    .contentType(ContentType.JSON)
      .body(productCreate)
    .when()
    .post("/products")
    .then().statusCode(201)

  }

  @Test
  void create_product_2() {

    //language=json
    def productCreate = '''
{
  "name": "Thinkpad T480s",
  "description": "A thinkpad, as simple as that"
}
'''

    given()
      .contentType(ContentType.JSON)
      .body(productCreate)
      .when()
      .post("/products")
      .then().statusCode(201)

  }

  @Test
  void find_product_by_id() {

    given()
      .contentType(ContentType.JSON)
      .when()
      .get("/products/1")
      .then().statusCode(200)
  }
}

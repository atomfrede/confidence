package com.gitlab.atomfrede.confidence.http;

import okhttp3.OkHttpClient;

public interface HttpClientFactory {

    OkHttpClient buildClient();

}

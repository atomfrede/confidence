package com.gitlab.atomfrede.confidence.web;

import com.gitlab.atomfrede.confidence.config.LocaleConfig;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@ControllerAdvice
public class LocaleControllerAdvice {

    @ModelAttribute("otherLocales")
    public List<Locale> getOtherLocales() {

        List<Locale> supported = LocaleConfig.supportedLocales;
        Locale currentLocale = LocaleContextHolder.getLocale();

        return supported.stream()
                .filter(l -> !l.equals(currentLocale))
                .collect(Collectors.toList());
    }
}

package com.gitlab.atomfrede.confidence.web;

import com.gitlab.atomfrede.confidence.service.Product;
import com.gitlab.atomfrede.confidence.web.gen.model.ProductCreateDto;
import com.gitlab.atomfrede.confidence.web.gen.model.ProductDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.WARN)
public abstract class ProductDtoMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(source = "name", target = "title")
    @Mapping(source = "description", target = "description")
    public abstract Product fromCreateDto(ProductCreateDto productCreateDto);

    @Mapping(source = "id", target = "id")
    @Mapping(source = "title", target = "name")
    @Mapping(source = "description", target = "description")
    public abstract ProductDto toDto(Product product);
}

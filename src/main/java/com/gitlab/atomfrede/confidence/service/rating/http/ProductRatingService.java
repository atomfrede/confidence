package com.gitlab.atomfrede.confidence.service.rating.http;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

import java.util.List;

public interface ProductRatingService {

    @GET("reviews")
    Call<List<ProductRatingDto>> listReviews();

    @GET("reviews")
    Call<List<ProductRatingDto>> findReviewsForProduct(@Query("productId") Long productId);

    @GET("reviews/{id}")
    Call<ProductRatingDto> getReview(@Path("id") Long id);
}

package com.gitlab.atomfrede.confidence.persistence;

import com.gitlab.atomfrede.confidence.persistence.gen.jooq.tables.records.ProductRecord;
import com.gitlab.atomfrede.confidence.service.Product;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class ProductRecordMapper {

    @Mapping(source = "id", target = "id")
    @Mapping(source = "description", target = "description")
    @Mapping(source = "title", target = "name")
    public abstract ProductRecord fromProduct(Product product);

    @Mapping(source = "id", target = "id")
    @Mapping(source = "description", target = "description")
    @Mapping(source = "name", target = "title")
    public abstract Product toProduct(ProductRecord record);
}

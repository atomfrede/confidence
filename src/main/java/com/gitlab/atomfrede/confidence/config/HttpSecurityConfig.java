package com.gitlab.atomfrede.confidence.config;

import com.gitlab.atomfrede.confidence.web.Routes;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

public class HttpSecurityConfig {

    private HttpSecurityConfig() {
        // private utility class constructor
    }

    public static void configure(HttpSecurity http, PersistentTokenRepository tokenRepository) throws Exception { //NOSONAR

        http
                .authorizeRequests()
                .antMatchers("/img/public/**", "/css/**", "/js/**", Routes.LOGIN).permitAll()
                .antMatchers("/v1/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin().loginPage(Routes.LOGIN).permitAll()
                .and()
                .logout().logoutSuccessUrl(Routes.LOGIN).permitAll()
                .and()
                .rememberMe().rememberMeParameter("remember-me").tokenRepository(tokenRepository);
    }
}

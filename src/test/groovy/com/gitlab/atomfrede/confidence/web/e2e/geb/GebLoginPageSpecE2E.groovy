package com.gitlab.atomfrede.confidence.web.e2e.geb

import com.gitlab.atomfrede.confidence.web.e2e.geb.pages.GebIndexPage
import com.gitlab.atomfrede.confidence.web.e2e.geb.pages.GebLoginPage
import geb.spock.GebReportingSpec

class GebLoginPageSpecE2E extends GebReportingSpec {

  def "login page can be rendered"() {

    given:
    to GebLoginPage
    report("login-page")
  }

  def "failed login is visualized"() {

    given:
    to GebLoginPage

    when: "user logs in with incorrect password"
    loginPage.login("user", "wrongpassword")

    then: "the user stays on the login page"
    at GebLoginPage

    and: "a generic error message is shown"
    $(".e2e__login-failed-message") != null

  }

  def "successful login forwards to index page"() {

    given:
    to GebLoginPage

    when: "user logs in"
    loginPage.login("user", "user")

    then: "the user is forwarded to index page"
    at GebIndexPage

  }
}

package com.gitlab.atomfrede.confidence.http;

import okhttp3.ConnectionPool;
import okhttp3.OkHttpClient;

import java.util.concurrent.TimeUnit;

public class OkHttpClientFactory implements HttpClientFactory {

    @Override
    public OkHttpClient buildClient() {

        return new OkHttpClient.Builder()
            .connectionPool(new ConnectionPool(5, 50, TimeUnit.SECONDS))
            .connectTimeout(100, TimeUnit.MILLISECONDS)
            .readTimeout(1000, TimeUnit.MILLISECONDS)
            .retryOnConnectionFailure(false)
            .build();
    }
}

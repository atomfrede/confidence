import com.gitlab.atomfrede.confidence.web.e2e.geb.GebContainerConfiguration
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.firefox.FirefoxDriver

baseUrl = "http://confidence:8080"
reportsDir = "build/geb-reports"

driver = {

  GebContainerConfiguration.instance.getFirefoxWebdriver()
}

environments {
  "chrome" {
    driver = {
      GebContainerConfiguration.instance.getChromeWebdriver()
    }
  }
  "firefoxLocal" {
    new FirefoxDriver()
  }
  "chromeLocale" {
    new ChromeDriver()
  }
}
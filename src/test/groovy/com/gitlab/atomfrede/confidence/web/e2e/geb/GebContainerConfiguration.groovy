package com.gitlab.atomfrede.confidence.web.e2e.geb

import com.gitlab.atomfrede.confidence.TestPaths
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.firefox.FirefoxOptions
import org.openqa.selenium.remote.RemoteWebDriver
import org.testcontainers.containers.BrowserWebDriverContainer
import org.testcontainers.containers.GenericContainer
import org.testcontainers.containers.Network
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.images.builder.ImageFromDockerfile

class GebContainerConfiguration {

  static getInstance() {
    return instance
  }

  static GebContainerConfiguration instance = new GebContainerConfiguration()

  BrowserWebDriverContainer firefoxContainer
  BrowserWebDriverContainer chromeContainer
  RemoteWebDriver firefox
  RemoteWebDriver chrome

  GebContainerConfiguration() {
    Network network = Network.SHARED

    new PostgreSQLContainer("postgres:11.0")
      .withDatabaseName("confidence")
      .withUsername("confidence")
      .withPassword("confidence")
      .withNetwork(network)
      .withNetworkAliases("postgres")
      .start()

    new GenericContainer(new ImageFromDockerfile().withDockerfileFromBuilder(
      { builder ->
        builder.from("openjdk:11-jdk-stretch")
          .volume("/tmp")
          .copy("confidence-0.0.1-SNAPSHOT.jar", "app.jar")
          .entryPoint("java", "-Dspring.datasource.password=confidence",
            "-Dspring.datasource.url=jdbc:postgresql://postgres:5432/confidence",
            "-Djava.security.egd=file:/dev/./urandom", "-jar", "/app.jar")
          .build()
      })
      .withFileFromPath("confidence-0.0.1-SNAPSHOT.jar", TestPaths.resolve("../../../build/libs/confidence-0.0.1-SNAPSHOT.jar")))
      .withExposedPorts(8080)
      .withNetwork(network)
      .withNetworkAliases("confidence")
      .start()

    firefoxContainer = new BrowserWebDriverContainer()
      .withCapabilities(new FirefoxOptions())
      .withNetwork(network)
      .withNetworkAliases("selenium") as BrowserWebDriverContainer

    chromeContainer = new BrowserWebDriverContainer()
      .withCapabilities(new ChromeOptions())
      .withNetwork(network)
      .withNetworkAliases("selenium") as BrowserWebDriverContainer
  }

  RemoteWebDriver getFirefoxWebdriver() {
    firefoxContainer.start()
    firefox = firefoxContainer.getWebDriver()
  }

  RemoteWebDriver getChromeWebdriver() {
    chromeContainer.start()
    chrome = chromeContainer.getWebDriver()
  }



}

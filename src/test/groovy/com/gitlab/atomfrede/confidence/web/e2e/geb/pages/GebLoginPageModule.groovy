package com.gitlab.atomfrede.confidence.web.e2e.geb.pages

import geb.Module
import geb.module.PasswordInput
import geb.module.TextInput

class GebLoginPageModule extends Module {

  static content = {
    usernameInput { $(  class: "e2e__username").module(TextInput) }
    passwordInput { $( class: "e2e__password").module(PasswordInput) }
    loginButton { $(class: "e2e__login-button") }
  }

  void login(username, password) {
    usernameInput.text = username
    passwordInput.text = password
    loginButton.click()

  }
}

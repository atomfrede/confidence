package com.gitlab.atomfrede.confidence.web

import com.gitlab.atomfrede.confidence.TestFilters
import com.gitlab.atomfrede.confidence.TestPaths
import io.restassured.RestAssured
import io.restassured.http.ContentType
import org.junit.Before
import org.junit.BeforeClass
import org.junit.ClassRule
import org.junit.Test
import org.testcontainers.containers.GenericContainer
import org.testcontainers.containers.Network
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.images.builder.ImageFromDockerfile

import static io.restassured.RestAssured.given

class ProductsApiContainerIT {

  @ClassRule
  public static PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer("postgres:11.0")
    .withDatabaseName("confidence")
    .withUsername("confidence")
    .withPassword("confidence")
    .withNetwork(Network.SHARED)
    .withNetworkAliases("postgres")

  public static GenericContainer bootJar;

  @BeforeClass
  static void setup() {

    RestAssured.reset()

    def testFilters = new TestFilters(TestPaths.resolve("../../../api-spec/server/products.yaml").toString())

    RestAssured.filters(
      testFilters.swaggerValidationFilter
    )

    RestAssured.enableLoggingOfRequestAndResponseIfValidationFails()

    bootJar = new GenericContainer(new ImageFromDockerfile().withDockerfileFromBuilder(
      { builder ->
        builder.from("openjdk:11-jdk-stretch")
          .volume("/tmp")
          .copy("confidence-0.0.1-SNAPSHOT.jar", "app.jar")
          .entryPoint("java", "-Dspring.datasource.password=confidence",
            "-Dspring.datasource.url=jdbc:postgresql://postgres:5432/confidence",
            "-Djava.security.egd=file:/dev/./urandom", "-jar", "/app.jar")
          .build()
      })
      .withFileFromPath("confidence-0.0.1-SNAPSHOT.jar", TestPaths.resolve("../../../build/libs/confidence-0.0.1-SNAPSHOT.jar")))
      .withExposedPorts(8080)
      .withNetwork(postgreSQLContainer.getNetwork())

    bootJar.start()

  }

  @Before
  void clear() {

    RestAssured.baseURI = "http://${bootJar.getContainerIpAddress()}:${bootJar.getMappedPort(8080)}/v1"
  }

  @Test
  void create_product() {

    //language=json
    def productCreate = '''
{
  "name": "Thinkpad T480s",
  "description": "A thinkpad, as simple as that"
}
'''

    given()
      .contentType(ContentType.JSON)
      .body(productCreate)
      .when()
      .post("/products")
      .then().statusCode(201)
  }

  @Test
  void getProduct() {

    given()
      .contentType(ContentType.JSON)
      .when()
      .get("/products/123")
      .then().statusCode(200)
  }


}

package com.gitlab.atomfrede.confidence.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Objects;

@Controller
public class PageRouter {

    @GetMapping(Routes.LOGIN)
    public String login(@RequestParam(name = "error", required = false) String error, Model model) {

        if (Objects.nonNull(error)) {
            model.addAttribute("loginError", true);
        }
        return Pages.LOGIN;
    }

    @GetMapping(Routes.ROOT)
    public String root(Model model) {

        return index(model);
    }

    @GetMapping(Routes.INDEX)
    public String index(Model model) {

        model.addAttribute("activeNav", "index/");
        return Pages.INDEX;
    }
}

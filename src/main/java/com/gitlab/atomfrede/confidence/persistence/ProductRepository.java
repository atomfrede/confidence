package com.gitlab.atomfrede.confidence.persistence;

import com.gitlab.atomfrede.confidence.Page;
import com.gitlab.atomfrede.confidence.service.Product;

import java.util.Optional;

public interface ProductRepository {

    Optional<Product> findOneById(Long id);

    Optional<Product> createProduct(Product product);

    void clearAll();

    Page<Product> findAll(Integer offset, Integer limit);
}

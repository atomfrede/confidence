package com.gitlab.atomfrede.confidence.web;

public final class Pages {

    private Pages() {
        // empty utility class constructor
    }

    public static final String LOGIN = "login/login";
    public static final String INDEX = "index/index";

}

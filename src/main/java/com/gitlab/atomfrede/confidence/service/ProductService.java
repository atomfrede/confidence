package com.gitlab.atomfrede.confidence.service;

import com.gitlab.atomfrede.confidence.persistence.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductService {

    private ProductRepository productRepository;

    @Autowired
    public ProductService(ProductRepository repository) {

        this.productRepository = repository;
    }

    public Product createProduct(Product product) {

        return productRepository.createProduct(product).orElseThrow(() -> new RuntimeException("Unable to create product."));
    }


}

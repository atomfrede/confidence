package com.gitlab.atomfrede.confidence;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Page<T> {

    private long totalCount;

    private List<T> items;

    private Page(final long totalCount, final List<T> items) {

        this.totalCount = totalCount;
        this.items = unmodifiableListOf(items);
    }

    private List<T> unmodifiableListOf(final List<T> items) {

        if (Objects.nonNull(items)) {
            return Collections.unmodifiableList(items);
        } else {
            return Collections.emptyList();
        }
    }

    public static <T> Page<T> from(final long totalCount, final List<T> items) {

        return new Page<>(totalCount, items);
    }

    public long getTotalCount() {

        return totalCount;
    }

    public List<T> getItems() {

        return items;
    }
}
